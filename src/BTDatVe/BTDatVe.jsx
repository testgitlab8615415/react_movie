import React from "react";
import ChairList from "./ChairList";
import Result from "./Result";
import data from "./data.json";

const BTDatVe = () => {
  return (
    <div className="container mt-5">
      <h1 style={{color:"pink"}} className="bg-dark col-8 ">NETFLIX AND CHILL</h1>
      <div className="row">
        <div className="col-8">
          <h1 className="display-4">CHỌN GHẾ ĐI</h1>
          <div className="text-center p-3 font-weight display-4 bg-dark text-white mt-3">
            SCREEN
          </div>
          <ChairList data={data} />
        </div>
        <div className="col-4">
          <Result />
        </div>
      </div>
    </div>
  );
};

export default BTDatVe;
