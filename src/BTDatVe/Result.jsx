import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingsAction, payAction } from "../store/action/chair";
import Swal from "sweetalert2";

const Result = () => {
  const { chairBookings } = useSelector((state) => state.chairReducer);
  const dispatch = useDispatch();
  const totalAmount = chairBookings.reduce(
    (total, ghe) => (total += ghe.gia),
    0
  );

  const formatCurrency = (amount) => {
    return amount.toLocaleString("vi-VN", {
      style: "currency",
      currency: "VND",
    });
  };
  const handleClick = () => {
    Swal.fire({
      position: "center",
      icon: "success",
      title: "CHÚC BẠN CÓ BUỔI XEM PHIM VUI VẺ",
      showConfirmButton: false,
      timer: 1500,
    });
  };
  return (
    <div>
      <h2>Danh sách ghế chọn</h2>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark chair booked">
          Ghế đã đặt
        </button>
        <button className="btn btn-outline-dark mt-3 chair booking">
          Ghế đang chọn
        </button>
        <button className="btn btn-outline-dark mt-3">Ghế Chưa đặt</button>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>Số ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {chairBookings.map((ghe) => (
            <tr>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia}</td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => {
                    dispatch(chairBookingsAction(ghe));
                  }}
                >
                  Hủy
                </button>
              </td>
            </tr>
          ))}

          {/* Tính tổng tiền */}
          <tr className="text-danger">
            <td>
              <b>Tổng tiền</b>
            </td>
            {/* <td>
                {chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}
              </td> */}
            <td>
              <b>{formatCurrency(totalAmount)}</b>
            </td>
          </tr>
        </tbody>
      </table>

      <button
        className="btn btn-success"
        onClick={() => {
          dispatch(payAction());
          handleClick();
        }}
      >
        Thanh toán
      </button>
    </div>
  );
};

export default Result;
