import { combineReducers } from "redux";
import { chairReducer } from "./reducer/chair";

export const rootReducer = combineReducers({
  chairReducer,
});
